﻿using HplusEcommerceWeb.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HplusEcommerceWeb.Controllers
{
    public class HomeController : Controller
    {
        [CrawlerFilter]
        public ActionResult Index()
        {
            //this is used to test exception codes
            //throw new System.Exception();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}