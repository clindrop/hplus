﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HplusEcommerceWeb.Controllers
{
    public class ProductsController : Controller
    {
        // GET: Products
        public ActionResult Index()
        {
            //this is to redirect it to another controller which is the product controller
            //this is idle when url is changed and you still want to maintain the old controller

            return RedirectToAction("Index", "Product");
        }
    }
}