﻿using HplusEcommerce.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace HplusEcommerceWeb.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        //public ActionResult Index(string username, string password)
        //{
        //    if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
        //    {
        //        FormsAuthentication.SetAuthCookie(username, false);
        //        return Redirect(FormsAuthentication.GetRedirectUrl(username, false));
        //    }

        //   // ViewBag.Failed = true;
        //    return View();
        //}

        public ActionResult Index(Login request)
        {
            if (!string.IsNullOrEmpty(request.Username) && !string.IsNullOrEmpty(request.Password))
            {
                FormsAuthentication.SetAuthCookie(request.Username, false);
                return Redirect(FormsAuthentication.GetRedirectUrl(request.Username, false));
            }

            // ViewBag.Failed = true;
            return View(request);
        }


    }
}