﻿using HplusEcommerceWeb.Filters;
using System.Web;
using System.Web.Mvc;

namespace HplusEcommerceWeb
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
           // filters.Add(new HandleErrorAttribute());
            filters.Add(new CustomExceptionHandler());
            filters.Add(new LogRequestFilter());

        }
    }
}
