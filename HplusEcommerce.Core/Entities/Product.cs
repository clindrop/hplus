﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HplusEcommerce.Core.Entities
{
    public class Product
    {
        public string Name { get; set; }
        public decimal FullPrice { get; set; }
        public decimal CurrentPrice { get; set; }
        public int PercentOff { get; set; }
        public string  ImagePath { get; set; }
        public int StarRating { get; set; }
    }
}
